#include "menu.h"

#include <stdio.h>

#include "init.h"

void charg_jeu(struct jeu *jeu, char *fname)
{
  FILE *f = fopen(fname, "r");
  fscanf(f, "%d %d,%d", &(jeu->joueur), &(jeu->prev_coord.x), &(jeu->prev_coord.y));
  for (int i = 0; i < 10; i++)
  {
    for (int j = 0; j < 10; j++)
    {
      fscanf(f, "%d-%d ", &(jeu->plateau[j][i].equipe), &(jeu->plateau[j][i].type));
    }
  }
}

int afficher_menu(struct jeu *jeu)
{
  int rep = REGLES;
  while (rep == REGLES)
  {
    printf("What do you what to do ?\n");
    printf("(1 : play, 2 : rules, 3 : quit, 4 : load)\n");
    scanf("%d", &rep);
  }
  if (rep == 4)
  {
    printf("nom du fichier : ");
    char fname[255];
    scanf("%s", fname);
    charg_jeu(jeu, fname);
  }
  else
    init_jeu(jeu);
  return rep;
}
