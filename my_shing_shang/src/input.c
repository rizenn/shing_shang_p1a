#include "input.h"

#include <stdio.h>
#include <string.h>

#include "structures.h"

Coord_dep input()
{
  char saisie[6];
  Coord_dep coord;
  printf("\nEntrez votre déplacement (ex: a6 a7 ) ou exit\n");
  do {
    fgets(saisie, 6, stdin);
    if (!strncmp(saisie, "exit", 4))
    {
      coord.coord1.x = -1;
      coord.coord1.y = QUITTER;
      break;
    }
    if (!strncmp(saisie, "save", 4))
    {
      coord.coord1.x = -1;
      coord.coord1.y = SAVE;
      break;
    }
    coord.coord1.x = saisie[0] - 'a';
    coord.coord2.x = saisie[3] - 'a';
    coord.coord1.y = saisie[1] - '0';
    coord.coord2.y = saisie[4] - '0';
  } while ((coord.coord1.x > 9) || (coord.coord1.x < 0)
      || (coord.coord1.y > 9) || (coord.coord1.y < 0)
      || (coord.coord2.x > 9) || (coord.coord2.x < 0)
      || (coord.coord2.y > 9) || (coord.coord2.y < 0)
      || saisie[2] != ' ');
  return coord;
  
}
