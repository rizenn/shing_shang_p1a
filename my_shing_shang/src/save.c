#include "save.h"
 
#include <stdio.h>

#include "structures.h"


void fprint_case(FILE *f, Case case_)
{
  fprintf(f, "%d-", case_.equipe);
  fprintf(f, "%d ", case_.type);
}

void sauv_jeu(struct jeu jeu)
{
  char fname[255];
  printf("entrer un nom de fichier : ");
  scanf("%s", fname);
  FILE *f = fopen(fname, "w");
  fprintf(f, "%d %d,%d\n", jeu.joueur, jeu.prev_coord.x, jeu.prev_coord.y);
  for (int i = 0; i < 10; i++)
  {
    for (int j = 0; j < 10; j++)
      fprint_case(f, jeu.plateau[j][i]);
    fprintf(f, "\n");
  }
}
