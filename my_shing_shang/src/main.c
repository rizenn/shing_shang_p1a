
#include "structures.h"
#include "menu.h"
#include "save.h"
#include "engine.h"
#include "affichage.h"


int main(void)
{
  while (1)
  {
    clrscr();
    struct jeu jeu;
    if (afficher_menu(&jeu) == QUITTER)
      // show_menu take game to be able to load a game
      break;
    int etat = QUITTER;
    while ((etat = jouer(&jeu)) != QUITTER)
    {
      if (etat == SAVE)
      {
        sauv_jeu(jeu);
        break;
      }
      else
        jeu.joueur = !jeu.joueur;
    }
  }
  return 0;
}
