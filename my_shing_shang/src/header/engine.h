#ifndef ENGINE_H
# define ENGINE_H

#include "structures.h"

int jouer(struct jeu *jeu);
int move_singe(Case plateau[][10], Coord_dep coord);
int move_lion(Case plateau[][10], Coord_dep coord);
int move_dragon(Case plateau[][10], Coord_dep coord);

#endif /* ENGINE_H */
