#ifndef MOVE_H
# define MOVE_H

#include "structures.h"

int move(struct jeu *jeu, Coord_dep coord);
int move_saut_allie(struct jeu *jeu, Coord_dep coord);
int move_saut_ennemy(struct jeu *jeu, Coord_dep coord);

#endif /* MOVE_H */
