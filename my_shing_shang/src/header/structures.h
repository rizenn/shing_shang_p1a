
/*******************************************************
Nom ......... : proto.h
Role ........ : Librairie des fonctions utilisées pour
le projet et définition des énumérations / structures
Auteur ...... : Jestin Gabriel / Auty James
********************************************************/
#ifndef STRUCTURES_H
# define STRUCTURES_H


/**************
** Enumérations
*/

enum etat_jeu {
  QUITTER,
  JOUER,
  SAVE,
  REGLES,
};

/* enumeration pour les differents types que peut prendre une case */
enum case_type
{
  INEXISTANTE,
  VIDE,
  PORTAIL,
  SINGE,
  LION,
  DRAGON
};

/* enumeration pour les differentes equipes que peut prendre une case */
enum equipe
{
  AUCUNE,
  BLEU,
  ROUGE
};

enum e_etat_jeu {
  PAS_JOUER,
  DEFAUT,
  SAUT_ALLIE,
  SAUT_ENNEMY
};

/************
** Structures
*/

/* structure definissant une case du plateau (un type et une équipe) */
typedef struct
{
  int type;
  int equipe;
} Case;

/* structure définissant une coordonnée */
typedef struct
{
  int x;
  int y;
} Coord;

/* structure définissant un couple de coordonnées */
typedef struct
{
  Coord coord1;
  Coord coord2;
} Coord_dep;

struct jeu {
  int joueur;
  int etat_jeu;
  Coord prev_coord;
  Case plateau[10][10];
};

#endif /* STRUCTURES_H */
