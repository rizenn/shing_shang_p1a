#ifndef AFFICHAGE_H
# define AFFICHAGE_H

#include "structures.h"

#include <stdio.h>

#define couleur(param) printf("\033[%sm",param)
// change la couleur du texte affiché dans la console apres l'appel de cette fonction
#define clrscr() printf("\033[H\033[2J") // vide la console

void afficher_plateau(Case plateau[][10]);

void afficher_jeu(struct jeu jeu);

#endif /* AFFICHAGE_H */
