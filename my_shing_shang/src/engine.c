#include "engine.h"
 
#include <math.h>
#include <stdio.h>

#include "input.h"
#include "move.h"
#include "structures.h"
#include "affichage.h"

int continuer_de_jouer()
{
  int r = 0;
  char choix[2];
  printf("\nVoulez vous continuer votre tour (o/n)?\n");
  do {
    fgets(choix, 2, stdin);
  } while ((choix[0] != 'o') && (choix[0] != 'n'));
  if (choix[0] == 'o')
  {
    r = 1;
  }
  else if (choix[0] == 'n')
  {
    r = 0;
  }
  return r;
}


void vider_case(Case plateau[10][10], int x, int y)
{
  plateau[x][y].type = VIDE;
  plateau[x][y].equipe = AUCUNE;
}

/*
** déplace une pièce d'une case à une autre
*/
void deplacer_piece(Case plateau[10][10], Coord_dep coord)
{
  plateau[coord.coord2.x][coord.coord2.y].type = plateau[coord.coord1.x][coord.coord1.y].type;
  plateau[coord.coord2.x][coord.coord2.y].equipe = plateau[coord.coord1.x][coord.coord1.y].equipe;
  plateau[coord.coord1.x][coord.coord1.y].type = VIDE;
  plateau[coord.coord1.x][coord.coord1.y].equipe = AUCUNE;
}


int move_dragon(Case plateau[10][10], Coord_dep coord)
{
  int r = PAS_JOUER;
  int x1 = coord.coord1.x;
  int y1 = coord.coord1.y;
  int x2 = coord.coord2.x;
  int y2 = coord.coord2.y;
  if (!((fabs(x1-x2) == 2) || (fabs(y1-y2) == 2))) // si le deplacement est de 2 cases
  {
    printf("Vous devez sauter une piece avec un DRAGON\n");
    return r;
  }
  if (!(!((x1-x2 == fabs(1)) && (y1-y2 == fabs(2)))
        && !((x1-x2 == fabs(2)) && (y1-y2 == fabs(1))))) // si la pièce ne change pas de direction pendant le saut
  {
    printf("Vous ne pouvez pas changer de direction au cours d'un saut\n");
    return r;
  }

  if (!((plateau[(x1+x2)/2][(y1+y2)/2].type == SINGE)  // si la case sautee est un SINGE
      || (plateau[(x1+x2)/2][(y1+y2)/2].type == LION)    // si la case sautee est un LION
      || (plateau[(x1+x2)/2][(y1+y2)/2].type == DRAGON))) // si la case sautee est un DRAGON
  {
    printf("Vous devez sauter une piece avec un DRAGON !\n");
    return r;
  }
  deplacer_piece(plateau, coord);
  r = SAUT_ALLIE;
  if (plateau[(x1+x2)/2][(y1+y2)/2].equipe != plateau[x2][y2].equipe) // si la case sautee n'est pas de la même equipe que le joueur
  {
    vider_case(plateau,(x1+x2)/2,(y1+y2)/2);
    r = SAUT_ENNEMY;
  }
  return r;
}

int move_lion(Case plateau[10][10], Coord_dep coord)
{
  int r = -1;
  int x1 = coord.coord1.x;
  int y1 = coord.coord1.y;
  int x2 = coord.coord2.x;
  int y2 = coord.coord2.y;
  if (!((fabs(x1-x2) == 2) || (fabs(y1-y2) == 2))) // si le deplacement est de 2 cases (l utilisateur veut effectuer un saut)
  {
    if ((fabs(x1-x2) <= 1) && fabs(y1-y2) <= 1) // si le déplacement est de 1 case
    {
      deplacer_piece(plateau, coord);
      r = 0;
    }
    else
    {
      printf("La case 2 est trop éloignée\n");
      return r;
    }
  }
  if (!(!((fabs(x1-x2) == 1) && (fabs(y1-y2) == 2)) && !((fabs(x1-x2) == 2) && (fabs(y1-y2) == 1)))) // si la pièce ne change pas de direction pendant le saut
  {
    printf("Déplacement impossible\n");
    return r;
  }
  if (!((plateau[(x1+x2)/2][(y1+y2)/2].type == SINGE)
   || (plateau[(x1+x2)/2][(y1+y2)/2].type == LION))) //si la case sautée est un SINGE ou un LION
  {
    printf("Déplacement impossible: un LION peut uniquement sauter au dessus d'un autre LION ou d'un SINGE\n");
    return r;
  }
  deplacer_piece(plateau, coord);
  r = SAUT_ALLIE;
  if (plateau[(x1+x2)/2][(y1+y2)/2].equipe != plateau[x2][y2].equipe)
    // si la case sautee n'est pas de la même equipe que le joueur
  {
    vider_case(plateau,(x1+x2)/2,(y1+y2)/2);
    r = SAUT_ENNEMY;
  }
  return r;
}


int move_singe(Case plateau[10][10], Coord_dep coord)
{
  int r = PAS_JOUER;
  int x1 = coord.coord1.x;
  int y1 = coord.coord1.y;
  int x2 = coord.coord2.x;
  int y2 = coord.coord2.y;
  if ((fabs(x1-x2) == 2) || (fabs(y1-y2) == 2)) // si le deplacement est de 2 cases
  {
    if (!((fabs(x1-x2) == 1) && (fabs(y1-y2) == 2)) && !((fabs(x1-x2) == 2) && (fabs(y1-y2) == 1)))
      // si le deplacement est bien lineaire
    {
      printf("Vous ne pouvez pas changer de direction au cours d'un saut\n");
      return r;
    }
    if ((plateau[(x1+x2)/2][(y1+y2)/2].type == INEXISTANTE) // si la case sautée est INEXISTANTE
        || (plateau[(x1+x2)/2][(y1+y2)/2].type == VIDE))        // si la case sautée est VIDE
    {
      deplacer_piece(plateau, coord);
      r = 0;
    }
    else if (plateau[(x1+x2)/2][(y1+y2)/2].type == SINGE)
    {
      deplacer_piece(plateau, coord);
      // changer_joueur(jo);
      r = SAUT_ALLIE;
      if (plateau[(x1+x2)/2][(y1+y2)/2].equipe != plateau[x2][y2].equipe) // si la piece sautée n'est pas de la mếme équipe que le joueur
      {
        vider_case(plateau,(x1+x2)/2,(y1+y2)/2);
        r = SAUT_ENNEMY;
      }
    }
  }
  else
  {
    if ((fabs(x1-x2) <= 2) && (fabs(y1-y2) <= 2)) // si le déplacement est de 1 case
    {
      deplacer_piece(plateau, coord);
      r = 0;
    }
    else
      printf("La case 2 est trop éloignée\n");
  }
  return r;
}

int jouer(struct jeu *jeu)
{
  printf("jeu\n");
  Coord_dep coord;
  do {
    afficher_jeu(*jeu);
    printf("ici\n");
    coord = input();
    if (coord.coord1.x == -1)
      return  coord.coord1.y;
    jeu->etat_jeu = move(jeu, coord);
  } while (jeu->etat_jeu == PAS_JOUER);
  do {
    switch (jeu->etat_jeu)
    {
      case SAUT_ALLIE:
        do {
          afficher_jeu(*jeu);
          if (continuer_de_jouer())
          {
            afficher_jeu(*jeu);
            coord = input();
            if (coord.coord1.x == -1)
              return coord.coord1.y;
            jeu->etat_jeu = move_saut_allie(jeu, coord);
          }
          else
            jeu->etat_jeu = DEFAUT;
        } while (jeu->etat_jeu == PAS_JOUER);
        break;
      case SAUT_ENNEMY:
        do {
          afficher_jeu(*jeu);
          if (continuer_de_jouer())
          {
            afficher_jeu(*jeu);
            coord = input();
            if (coord.coord1.x == -1)
              return coord.coord1.y;
            jeu->etat_jeu = move_saut_ennemy(jeu, coord);
          }
          else
            jeu->etat_jeu = DEFAUT;
        } while (jeu->etat_jeu == PAS_JOUER);
        break;
    }
  } while (jeu->etat_jeu != DEFAUT);
  return REGLES;
}
