
/*******************************************************
Nom ......... : move.c
Role ........ : fonctions liées aux déplacements de pièces
Auteur ...... : Jestin Gabriel / Auty James
********************************************************/

#include "move.h"

#include "affichage.h"
#include "engine.h"
#include "structures.h"

#include <stdio.h>
#include <string.h>
#include <math.h> /* fabs */

/* définit le deplacement d'une pièce au début d'un tour */
int move(struct jeu *jeu, Coord_dep coord)
{
  int r = PAS_JOUER;
  if (!(jeu->plateau[coord.coord1.x][coord.coord1.y].equipe == jeu->joueur))
    // verifie que la piece jouées est bien de l'equipe du joueur courant
    printf("Vous avez sélectionner une pièce de la mauvaise équipe\n");
  else if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == SINGE
      && jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE)
    // applique les règles de déplacement des singes si la piece est un SINGE
    r = move_singe(jeu->plateau, coord);
  else if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == LION
      && jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE)
    // applique les règles de déplacement des lions si la piece est un LION
    r = move_lion(jeu->plateau, coord);
  else if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == DRAGON
      && jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE)
    // applique les règles de déplacement des dragons si la piece est un DRAGON
    r = move_dragon(jeu->plateau, coord);
  else
    printf("Vous devez selectionner une piece\n");
  if (r != PAS_JOUER)
  {
    jeu->prev_coord.x = coord.coord2.x;
    jeu->prev_coord.y = coord.coord2.y;
  }
  return r;
}

/* définit le deplacement après un saut sur une pièce alliée */
int move_saut_allie(struct jeu *jeu, Coord_dep coord)
{
  int r = PAS_JOUER;
  if (!((coord.coord1.x == jeu->prev_coord.x)
   && (coord.coord1.y == jeu->prev_coord.y)
   && (jeu->plateau[coord.coord1.x][coord.coord1.y].equipe == jeu->joueur))) // verifie que la piece jouées est la même pièce qu'au précédent coup
  {
    printf("Vous avez sélectionner la mauvaise pièce\n");
    return r;
  }
  if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == SINGE
  && (jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE))
    // applique les règles de déplacement des singes si la piece est un SINGE
    r = move_singe(jeu->plateau, coord);
  else if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == LION
  && (jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE))
    // applique les règles de déplacement des singes si la piece est un SINGE
    r = move_lion(jeu->plateau, coord);
  else if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == DRAGON
  && (jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE))
    // applique les règles de déplacement des singes si la piece est un SINGE
    r = move_dragon(jeu->plateau, coord);
  else
    printf("Vous devez selectionner une piece\n");
  if (r > -1)
  {
    jeu->prev_coord.x = coord.coord2.x;
    jeu->prev_coord.y = coord.coord2.y;
  }
  return r;
}

/* définit le deplacement d'une pièce après un saut sur une pièce ennemie */
int move_saut_ennemy(struct jeu *jeu, Coord_dep coord)
{
  int r = -1;
  if (!(((coord.coord1.x != jeu->prev_coord.x)
      || (coord.coord1.y != jeu->prev_coord.y))
      && (jeu->plateau[coord.coord1.x][coord.coord1.y].equipe == jeu->joueur))) // verifie que la piece jouée n'est pas la même pièce qu'au précédent coup
  {
    printf("Vous avez sélectionner la mauvaise pièce\n");
    return r;
  }
  if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == SINGE
  && (jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE))
    // applique les règles de déplacement des singes si la piece est un SINGE
    r = move_singe(jeu->plateau, coord);
  else if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == LION
  && (jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE))
    // applique les règles de déplacement des singes si la piece est un SINGE
    r = move_lion(jeu->plateau, coord);
  else if (jeu->plateau[coord.coord1.x][coord.coord1.y].type == DRAGON
  && (jeu->plateau[coord.coord2.x][coord.coord2.y].type == VIDE))
    // applique les règles de déplacement des singes si la piece est un SINGE
    r = move_dragon(jeu->plateau, coord);
  else
    printf("Vous devez selectionner une piece\n");
  if (r > -1)
  {
    jeu->prev_coord.x = coord.coord2.x;
    jeu->prev_coord.y = coord.coord2.y;
  }
  return r;
}

